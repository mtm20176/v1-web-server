# Coder v1 Web Server example

## Getting Started

This repo is an example of using Coder's [`Open in Coder`](https://coder.com/docs/v1/latest/images/embed-button) button to instantly onboard a developer or data scientist.

The example will create a container in a Kubernetes cluster namespace, create a dev URL and start a Python web server.

#### demo-2.cdr.dev
[![Open in Coder demo-2](https://demo-2.cdr.dev/static/image/embed-button.svg)](https://demo-2.cdr.dev/workspaces/git?org=default&image=63eae7d6-28f477bcc860329a035ca2cd&tag=latest&service=63eaf35f-2abdfb90606d11a5373980cd&repo=git@gitlab.com:mtm20176/v1-web-server.git)

#### clean.demo.coder.com
[![Open in Coder clean](https://clean.demo.coder.com/static/image/embed-button.svg)](https://clean.demo.coder.com/workspaces/git?org=default&image=63eaefdd-047b63aab9483d5abcefe1d0&tag=latest&service=63eaeec4-03d9e19017e72b025d6111d0&repo=git@gitlab.com:mtm20176/v1-web-server.git)

## Container image contents
1. Coder's example `enterprise-base` image which includes python3 - [see Dockerfile](https://github.com/coder/enterprise-images/blob/main/images/base/Dockerfile.ubuntu)
1. Copies the [`configure` script](https://coder.com/docs/v1/latest/images/configure) to the `/coder` directory - this is run after the workspace container is built.
1. Copies a sample index.html file to the `/coder` directory in the workspace container

## Configure script actions
1. Creates a `dev URL` to show the web server contents in a browser
1. Copies the `index.html` to the user's home directory `/home/coder` for the web-server to read.
1. Starts the Python web server on port `8888`

See the [Dockerfile](https://github.com/sharkymark/dockerfiles/blob/main/v1/coder-training/web-server/Dockerfile) for this custom image

